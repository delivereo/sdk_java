# Delivereo SDK Java

Delivereo Java SDK Library

This is the official SDK library used to integrate with Delivereo's API.
You can find more information about our API in the [swagger documentation link](https://www.delivereo.com/swagger-ui.html)
or in the API [docs documentation link](https://delivereo.docs.stoplight.io)

## Installation

Install the latest version with

# Maven 

```bash
<dependency>
  <groupId>com.delivereo</groupId>
  <artifactId>sdk</artifactId>
  <version>1.0.11</version>
</dependency>
```

# Gradle 

```bash
implementation 'com.delivereo:sdk:1.0.11'
```

## Basic Usage / Example

```java
import com.delivereo.org.BusinessClient;
import com.delivereo.org.base.BasicResponse;
import com.delivereo.org.enums.*;
import com.delivereo.org.request.calculate.CalculateAddress;
import com.delivereo.org.request.calculate.CalculatePoint;
import com.delivereo.org.request.create.CreateAddress;
import com.delivereo.org.request.create.CreateOrder;
import com.delivereo.org.request.create.CreateOrderItem;
import com.delivereo.org.request.create.CreatePoint;
import com.delivereo.org.response.calculate.CalculateResponse;
import com.delivereo.org.response.create.CreateResponse;
import com.delivereo.org.response.detail.BookingResponse;
import com.delivereo.org.response.login.SignInResponse;
import com.delivereo.org.response.login.TokenRenewalResponse;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Example {

    public static void main(String[] args) {
        try {
            // Crear una instancia del cliente de business. En server environment deberia apuntar a TEST
            BusinessClient businessClient = new BusinessClient(ServerEnvironment.STAGING, Language.SPANISH);

            // Logearse con las credenciales,
            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");

            // Si el login es exitoso, podemos usar el JWT token de la respuesta de login para poder ejecutar cualquier otra
            // accion en el API
            if (loginResponse.isStatus()) {
                // TOMAR EN CUENTA: Si el JWT token expira (al mandar una peticion que no sea login o renew token te devuelve codigo 403)
                // Se puede generar un nuevo token volviendose a logear o usando el metodo siguiente
                TokenRenewalResponse renewTokenResponse = businessClient.tokenRenewal(loginResponse.getJwtToken(), "info@delivereo.com");

                if (renewTokenResponse.isStatus()) {
                    System.out.println("Nuevo jwt Token: " + renewTokenResponse.getResult());
                    loginResponse.setJwtToken(renewTokenResponse.getResult());
                } else {
                    System.out.println("Nuevo JWT Token status: " + renewTokenResponse.getMessage());
                }

                // Calcular el costo de una carrera usando puntos geograficos
                List<CalculatePoint> points = new LinkedList<>();
                points.add(new CalculatePoint(1, BigDecimal.valueOf(-0.19045013), BigDecimal.valueOf(-78.48059853)));
                points.add(new CalculatePoint(2, BigDecimal.valueOf(-0.172213), BigDecimal.valueOf(-78.486467)));
                points.add(new CalculatePoint(3, BigDecimal.valueOf(-0.20963146), BigDecimal.valueOf(-78.43948563)));

                CalculateResponse calculateResponse = businessClient.calculateBookingCostUsingPoints(loginResponse.getJwtToken(), City.QUITO, points);

                if (calculateResponse.isStatus()) {
                    System.out.println("Costo con puntos geograficos: " + calculateResponse.getTotalAmount());
                }

                // Calcular el costo de una carrera usando direcciones
                List<CalculateAddress> addresses = new LinkedList<>();
                addresses.add(new CalculateAddress(1, "Amazonas y Veintimilla", "", "", "EC"));
                addresses.add(new CalculateAddress(2, "Francisco Dalmau y Calle 1", "", "", "EC"));

                CalculateResponse calculateResponseAddress = businessClient.calculateBookingCostUsingAddresses(loginResponse.getJwtToken(), City.QUITO, addresses);
                if (calculateResponse.isStatus()) {
                    System.out.println("Costo con direcciones: " + calculateResponseAddress.getTotalAmount());
                }

                // No es necesario calcular el costo de una carrera si se va a crear una. Es solo referencial
                // De igual manera se puede crear una carrera usando puntos geograficos o direcciones, la mejor opcion es
                // usar direcciones por facilidad

                // Crear carrera usando puntos geograficos
                List<CreatePoint> pointsCreate = new LinkedList<>();
                pointsCreate.add(new CreatePoint(1, BigDecimal.valueOf(-0.19045013), BigDecimal.valueOf(-78.48059853), "Jorge Cardenas", "Amazonas y Veintimilla E32A-32", "Edificio Tez Apartamento 200"));
                pointsCreate.add(new CreatePoint(2, BigDecimal.valueOf(-0.172213), BigDecimal.valueOf(-78.486467), "Marcelo Bonilla", "Diego de Vazquez y Bellavista E34-23", "Condominio Carmen 2 casa 34"));
                pointsCreate.add(new CreatePoint(3, BigDecimal.valueOf(-0.20963146), BigDecimal.valueOf(-78.43948563), "Pablo Mancero", "Naciones Unidas e Inaquito Esquina", "Edificio Metropolitan Oficina 409"));

                CreateOrder createOrder = new CreateOrder(
                        "123456",
                        new LinkedList<CreateOrderItem>(),
                        BigDecimal.valueOf(10),
                        BigDecimal.valueOf(1.2),
                        BigDecimal.valueOf(11.2),
                        PaymentMode.CREDIT_CARD
                );

                CreateResponse createResponse = businessClient.createBookingUsingPoints(loginResponse.getJwtToken(), City.QUITO, pointsCreate,
                        "Compra hamburguesa de pollo y hamburguesa de carne", BigDecimal.valueOf(11.2), createOrder,
                        "", "", "", "");

                if (createResponse.isStatus()) {
                    System.out.println("Nuevo Booking id con puntos geograficos: " + createResponse.getBookingId());
                }

                List<CreateAddress> createAddresses = new LinkedList<>();
                createAddresses.add(new CreateAddress(1, "Amazonas y Veintimilla", "", "", "EC", "Jorge Cardenas", "Amazonas y Veintimilla E32A-32", "Edificio Tez Apartamento 200"));
                createAddresses.add(new CreateAddress(2, "Francisco Dalmau y Calle 1", "", "", "EC", "Marcelo Bonilla", "Diego de Vazquez y Bellavista E34-23", "Condominio Carmen 2 casa 34"));

                CreateOrder createAdressOrder = new CreateOrder(
                        "123456",
                        new LinkedList<CreateOrderItem>(),
                        BigDecimal.valueOf(10),
                        BigDecimal.valueOf(1.2),
                        BigDecimal.valueOf(11.2),
                        PaymentMode.CREDIT_CARD
                );

                CreateResponse createAddressResponse = businessClient.createBookingUsingAddresses(loginResponse.getJwtToken(), City.QUITO, createAddresses,
                        "Compra hamburguesa de pollo y hamburguesa de carne", BigDecimal.valueOf(11.2), createAdressOrder,
                        "", "", "", "");

                if (createAddressResponse.isStatus()) {
                    System.out.println("Nuevo Booking id con direcciones: " + createAddressResponse.getBookingId());
                }

                // Crear carrera de tipo booking domicile
                CreateResponse createBookingDomicileResponse = businessClient.CreateBookingDomicilePharmacy(loginResponse.getJwtToken(), "CCI Quito",
                        new LinkedList<>(), "stefano@delivereo.com", "12345", "1715245933", "Daniel Mancero",
                        "Amazonas y naciones unidas", "+593-123456789", "Edificio Argos", PaymentMode.CREDIT_CARD, "EC");

                if (createBookingDomicileResponse.isStatus()) {
                    System.out.println("Nuevo Booking id domicile: " + createBookingDomicileResponse.getBookingId());
                }

                // Crear carrer de tipo booking domicile pharmacy
                CreateResponse createBookingDomicilePharmacyResponse = businessClient.CreateBookingDomicilePharmacy(loginResponse.getJwtToken(), "CCI Quito",
                        new LinkedList<>(Collections.singletonList("Inca Quito")), "stefano@delivereo.com", "12345", "1715245933", "Daniel Mancero",
                        "Amazonas y naciones unidas", "+593-123456789", "Edificio Argos", PaymentMode.CREDIT_CARD, "EC");

                if (createBookingDomicilePharmacyResponse.isStatus()) {
                    System.out.println("Nuevo Booking id domicile pharmacy: " + createBookingDomicilePharmacyResponse.getBookingId());
                }

                // Para obtener el detalle de una carrera se puede utilizar el siguiente metodo
                BookingResponse bookingResponse = businessClient.getBookingFullDetailById(
                        loginResponse.getJwtToken(),
                        (int) createAddressResponse.getBookingId());

                if (bookingResponse.isStatus()) {
                    System.out.println("Detalle Booking con direccion - descripcion: " + bookingResponse.getDescription());
                }

                // Si una carrera no puede ser atendida por alguno de nuestro delivererers, se puede reintentar
                // usando el siguiente metodo
                BasicResponse retryResponse = businessClient.retryBookingById(
                        loginResponse.getJwtToken(),
                        (int) createAddressResponse.getBookingId());

                System.out.println("Reintentar Booking con direccion, estado: " + retryResponse.getMessage());

                // Despues de que la carrera fue atendida por un deliverer, esta puede ser calificada usando el
                // siguiente metodo. No es obligatorio el calificar el conductor, pero se recomienda hacerlo si es
                // posible. En una nueva version del API se va a generar una calificacion automatica, quitando esta
                // opcion del API
                BasicResponse rateDriverResponse = businessClient.rateDriverForBookingById(
                        loginResponse.getJwtToken(),
                        (int) createAddressResponse.getBookingId(),
                        BigDecimal.valueOf(5.0),
                        RatingLowReason.NONE,
                        "Everything OK");

                System.out.println("Calificar Conductor Booking con direccion, estado: " + rateDriverResponse.getMessage());

                // Para cancelar una carrera, se puede utilizar el siguiente metodo
                BasicResponse cancelResponse = businessClient.cancelBookingById(
                        loginResponse.getJwtToken(),
                        (int) createResponse.getBookingId(),
                        CancelReason.NOT_NEEDED_ANYMORE,
                        "Another option was found");

                System.out.println("Cancelar Booking con puntos geograficos, estado: " + cancelResponse.getMessage());

                BasicResponse cancelAddressResponse = businessClient.cancelBookingById(
                        loginResponse.getJwtToken(),
                        (int) createAddressResponse.getBookingId(),
                        CancelReason.NOT_NEEDED_ANYMORE,
                        "Another option was found");

                System.out.println("Cancelar Booking con direccion, estado: " + cancelAddressResponse.getMessage());
            } else {
                // Hubo problemas al logearse no podemos hacer nada mas
                System.out.println(loginResponse.getMessage());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

```
