import com.delivereo.org.BusinessClient;
import com.delivereo.org.base.BasicResponse;
import com.delivereo.org.enums.*;
import com.delivereo.org.request.calculate.CalculateAddress;
import com.delivereo.org.request.calculate.CalculatePoint;
import com.delivereo.org.request.create.CreateAddress;
import com.delivereo.org.request.create.CreateOrder;
import com.delivereo.org.request.create.CreateOrderItem;
import com.delivereo.org.request.create.CreatePoint;
import com.delivereo.org.response.calculate.CalculateResponse;
import com.delivereo.org.response.create.CreateResponse;
import com.delivereo.org.response.detail.BookingResponse;
import com.delivereo.org.response.login.SignInResponse;
import com.delivereo.org.response.login.TokenRenewalResponse;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Example {

    public static void main(String[] args) {
        try {
            // Crear una instancia del cliente de business. En server environment deberia apuntar a TEST
            BusinessClient businessClient = new BusinessClient(ServerEnvironment.DEVELOPMENT, Language.SPANISH);

            // Logearse con las credenciales,
            SignInResponse loginResponse = businessClient.login("4D1D2127-00D", "1800medicity@farmaenlace.com", "1791984722001");

            // Si el login es exitoso, podemos usar el JWT token de la respuesta de login para poder ejecutar cualquier otra
            // accion en el API
            if (loginResponse.isStatus()) {
                // Crear carrer de tipo booking domicile pharmacy
                CreateResponse createBookingDomicilePharmacyResponse = businessClient.CreateBookingDomicilePharmacy(loginResponse.getJwtToken(), "QUITO - MEDICITY LA PRENSA",
                        new LinkedList<>(Collections.singletonList("QUITO - MEDICITY LA CONCEPCION")), "diegoquinonez@farmaenlace.com", "12345", "1715245933", "Daniel Mancero",
                        "Amazonas y naciones unidas", "+593-123456789", "Edificio Argos", PaymentMode.MIXED, "EC");

                if (createBookingDomicilePharmacyResponse.isStatus()) {
                    System.out.println("Nuevo Booking id domicile pharmacy: " + createBookingDomicilePharmacyResponse.getBookingId());
                }

                // Para obtener el detalle de una carrera se puede utilizar el siguiente metodo
                BookingResponse bookingResponse = businessClient.getBookingFullDetailById(
                        loginResponse.getJwtToken(),
                        (int) createBookingDomicilePharmacyResponse.getBookingId());

                if (bookingResponse.isStatus()) {
                    System.out.println("Detalle Booking con direccion - descripcion: " + bookingResponse.getDescription());
                }
            } else {
                // Hubo problemas al logearse no podemos hacer nada mas
                System.out.println(loginResponse.getMessage());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
