package com.delivereo.org;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.base.BasicResponse;
import com.delivereo.org.enums.*;
import com.delivereo.org.request.calculate.CalculateAddress;
import com.delivereo.org.request.calculate.CalculateBooking;
import com.delivereo.org.request.calculate.CalculatePoint;
import com.delivereo.org.request.cancel.CancelBooking;
import com.delivereo.org.request.create.*;
import com.delivereo.org.request.detail.DetailBooking;
import com.delivereo.org.request.login.SignIn;
import com.delivereo.org.request.login.TokenRenewal;
import com.delivereo.org.request.rate.RateDriver;
import com.delivereo.org.request.retry.RetryBooking;
import com.delivereo.org.response.calculate.CalculateResponse;
import com.delivereo.org.response.create.CreateResponse;
import com.delivereo.org.response.detail.BookingResponse;
import com.delivereo.org.response.login.SignInResponse;
import com.delivereo.org.response.login.TokenRenewalResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class BusinessClient {

    private ServerEnvironment serverEnvironment;
    private Language language;
    private OkHttpClient client;
    private Gson gson;
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    /// <summary>
    /// Basic constructor
    /// </summary>
    /// <param name="serverEnvironment">Server environment to choose</param>
    /// <param name="language">Language of the requests</param>
    public BusinessClient(ServerEnvironment serverEnvironment, Language language) {
        this.serverEnvironment = serverEnvironment;
        this.language = language;
        this.client = new OkHttpClient();
        this.gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd kk:mm:ss").create();
        ;
    }

    /// <summary>
    /// Login to API
    /// </summary>
    /// <param name="apiKey">Business api key</param>
    /// <param name="businessEmail">Business email</param>
    /// <param name="businessRuc">Business ruc</param>
    /// <returns>Sign in response</returns>
    public SignInResponse login(String apiKey, String businessEmail, String businessRuc) throws IOException {
        SignIn body = new SignIn(language, apiKey, businessEmail, businessRuc);
        return getResult(BusinessConstants.LOGIN_URL, "", body, true, SignInResponse.class);
    }

    /// <summary>
    /// Token renewal
    /// </summary>
    /// <param name="oldJwtToken">Old JWT Token</param>
    /// <returns></returns>
    public TokenRenewalResponse tokenRenewal(String oldJwtToken, String businessEmail) throws IOException {
        TokenRenewal body = new TokenRenewal(language, oldJwtToken, businessEmail);
        return getResult(BusinessConstants.TOKEN_RENEWAL_URL, "", body, true, TokenRenewalResponse.class);
    }

    /// <summary>
    /// Calculate booking using geographic points
    /// </summary>
    /// <param name="jwtToken">Jwt token</param>
    /// <param name="cityType">City</param>
    /// <param name="points">Geographic points</param>
    /// <returns>Calculate response</returns>
    public CalculateResponse calculateBookingCostUsingPoints(String jwtToken, City cityType, List<CalculatePoint> points) throws IOException {
        CalculateBooking body = new CalculateBooking(language, PackageSize.SMALL, cityType, points, new LinkedList<CalculateAddress>());
        return getResult(BusinessConstants.CALCULATE_URL, jwtToken, body, false, CalculateResponse.class);
    }

    /// <summary>
    /// Calculate booking using addresses
    /// </summary>
    /// <param name="jwtToken">Jwt token</param>
    /// <param name="cityType">City</param>
    /// <param name="addresses">Addresses</param>
    /// <returns>Calculate response</returns>
    public CalculateResponse calculateBookingCostUsingAddresses(String jwtToken, City cityType, List<CalculateAddress> addresses) throws IOException {
        CalculateBooking body = new CalculateBooking(language, PackageSize.SMALL, cityType, new LinkedList<CalculatePoint>(), addresses);
        return getResult(BusinessConstants.CALCULATE_URL, jwtToken, body, false, CalculateResponse.class);
    }

    /// <summary>
    /// Create booking using points
    /// </summary>
    /// <param name="jwtToken">Jwt token</param>
    /// <param name="cityType">City</param>
    /// <param name="points">Geographic points</param>
    /// <param name="description">Description of the booking</param>
    /// <param name="orderTotal">Total amount of the order including taxes</param>
    /// <param name="order">Order detail</param>
    /// <param name="shopName">Shop name (optional)(</param>
    /// <param name="shopIcon">Shop icon as url (optional)</param>
    /// <param name="bookingBusinessUserEmail">Booking owner business user email (optional)</param>
    /// <param name="extraInstructions">Extra instructions of the booking (optional)</param>
    /// <returns>Create response</returns>
    public CreateResponse createBookingUsingPoints(String jwtToken, City cityType, List<CreatePoint> points,
                                                   String description, BigDecimal orderTotal, CreateOrder order, String shopName, String shopIcon,
                                                   String extraInstructions, String bookingBusinessUserEmail) throws IOException {
        CreateBooking body = new CreateBooking(language, PackageSize.SMALL, cityType, points, new LinkedList<CreateAddress>(), description,
                "", shopName, shopIcon, orderTotal, extraInstructions, null, order, bookingBusinessUserEmail);
        return getResult(BusinessConstants.CREATE_URL, jwtToken, body, false, CreateResponse.class);
    }

    /// <summary>
    /// Create booking using addresses
    /// </summary>
    /// <param name="jwtToken">Jwt token</param>
    /// <param name="cityType">City</param>
    /// <param name="addresses">Addresses</param>
    /// <param name="description">Description of the booking</param>
    /// <param name="orderTotal">Total amount of the order including taxes</param>
    /// <param name="order">Order detail</param>
    /// <param name="shopName">Shop name (optional)(</param>
    /// <param name="shopIcon">Shop icon as url (optional)</param>
    /// <param name="bookingBusinessUserEmail">Booking owner business user email (optional)</param>
    /// <param name="extraInstructions">Extra instructions of the booking (optional)</param>
    /// <returns>Create response</returns>
    public CreateResponse createBookingUsingAddresses(String jwtToken, City cityType, List<CreateAddress> addresses,
                                                      String description, BigDecimal orderTotal, CreateOrder order, String shopName, String shopIcon,
                                                      String extraInstructions, String bookingBusinessUserEmail) throws IOException {
        CreateBooking body = new CreateBooking(language, PackageSize.SMALL, cityType, new LinkedList<CreatePoint>(), addresses, description,
                "", shopName, shopIcon, orderTotal, extraInstructions, null, order, bookingBusinessUserEmail);
        return getResult(BusinessConstants.CREATE_URL, jwtToken, body, false, CreateResponse.class);
    }

    /// <summary>
    /// Create booking domicile/domicile pharmacy
    /// </summary>
    /// <param name="jwtToken">Jwt token</param>
    /// <param name="businessFirstBranchName">Business first branch name</param>
    /// <param name="businessBranchNamesToTransfer">Business branches names to transfer</param>
    /// <param name="businessUserEmail">Business user email</param>
    /// <param name="orderReference">Order reference</param>
    /// <param name="clientPersonalId">Client personal id</param>
    /// <param name="clientFullName">Client full name</param>
    /// <param name="clientAddress">Client address</param>
    /// <param name="clientMobileNumber">Client mobile number</param>
    /// <param name="clientReference">Client reference</param>
    /// <param name="clientPaymentMode">Client payment mode</param>
    /// <param name="countryCode">Country code</param>
    /// <returns>Create response</returns>
    public CreateResponse CreateBookingDomicilePharmacy(String jwtToken, String businessFirstBranchName, List<String> businessBranchNamesToTransfer, String businessUserEmail, String orderReference, String clientPersonalId, String clientFullName, String clientAddress, String clientMobileNumber, String clientReference, PaymentMode clientPaymentMode, String countryCode) throws IOException {
        CreateBookingDomicilePharmacy body = new CreateBookingDomicilePharmacy(language, businessFirstBranchName, businessBranchNamesToTransfer,
                businessUserEmail, orderReference, clientPersonalId, clientFullName, clientAddress, clientMobileNumber, clientReference, clientPaymentMode, countryCode);
        return getResult(BusinessConstants.CREATE_URL_DOMICILE_PHARMACY_URL, jwtToken, body, false, CreateResponse.class);
    }

    /// <summary>
    /// Cancel booking by id
    /// </summary>
    /// <param name="jwtToken">Jwt token</param>
    /// <param name="bookingId">Booking id</param>
    /// <param name="cancelReason">Cancel reason</param>
    /// <param name="cancelFeedback">Cancel feedback</param>
    /// <returns>Basic response</returns>
    public BasicResponse cancelBookingById(String jwtToken, int bookingId, CancelReason cancelReason, String cancelFeedback) throws IOException {
        CancelBooking body = new CancelBooking(language, bookingId, cancelReason, cancelFeedback);
        return getResult(BusinessConstants.CANCEL_URL, jwtToken, body, false, BasicResponse.class);
    }

    /// <summary>
    /// Retry booking by id
    /// </summary>
    /// <param name="jwtToken">Jwt token</param>
    /// <param name="bookingId">Booking id</param>
    /// <returns>Basic response</returns>
    public BasicResponse retryBookingById(String jwtToken, int bookingId) throws IOException {
        RetryBooking body = new RetryBooking(language, bookingId);
        return getResult(BusinessConstants.RETRY_URL, jwtToken, body, false, BasicResponse.class);
    }

    /// <summary>
    /// Rate driver by booking id
    /// </summary>
    /// <param name="jwtToken">Jwt token</param>
    /// <param name="bookingId">Booking id</param>
    /// <param name="rating">Rating of the booking (1-5)</param>
    /// <param name="ratingLowReason">Rating low reason</param>
    /// <param name="ratingLowFeedback">Rating low feedback</param>
    /// <returns>Basic response</returns>
    public BasicResponse rateDriverForBookingById(String jwtToken, int bookingId, BigDecimal rating, RatingLowReason ratingLowReason, String ratingLowFeedback) throws IOException {
        RateDriver body = new RateDriver(language, bookingId, rating, ratingLowReason, ratingLowFeedback);
        return getResult(BusinessConstants.RATE_DRIVER_URL, jwtToken, body, false, BasicResponse.class);
    }

    /// <summary>
    /// Get full booking detail by booking id
    /// </summary>
    /// <param name="jwtToken">Jwt token</param>
    /// <param name="bookingId">Booking id</param>
    /// <returns>Booking response</returns>
    public BookingResponse getBookingFullDetailById(String jwtToken, int bookingId) throws IOException {
        DetailBooking body = new DetailBooking(language, bookingId);
        return getResult(BusinessConstants.DETAIL_URL, jwtToken, body, false, BookingResponse.class);
    }

    /// <summary>
    /// Get result from request using okhttp3
    /// </summary>
    /// <typeparam name="T">Basic response</typeparam>
    /// <typeparam name="K">Basic request</typeparam>
    /// <param name="url">Url for the request</param>
    /// <param name="jwtToken">JWT token for the request</param>
    /// <param name="body">Body for the request></param>
    /// <param name="basicAuth">Use basic auth</param>
    /// <returns></returns>
    private <T extends BasicResponse, K extends BasicRequest> T getResult(String url, String jwtToken, K body, boolean basicAuth, Class<T> type) throws IOException {
        T response = null;

        if (basicAuth) {
            RequestBody requestBody = RequestBody.create(JSON, gson.toJson(body));
            Request request = new Request.Builder()
                    .url(serverEnvironment.getUrl() + url)
                    .post(requestBody)
                    .addHeader("Authorization", "Basic c2VydmljZXMubW9iaWxlQGRlbGl2ZXJlby5jb206ZGVsaXZlcmVvLyotK2FiYzEyMw==")
                    .build();

            Response result = client.newCall(request).execute();
            if (result.body() != null) {
                response = gson.fromJson(result.body().string(), type);
            }
        } else {
            RequestBody requestBody = RequestBody.create(JSON, gson.toJson(body));
            Request request = new Request.Builder()
                    .url(serverEnvironment.getUrl() + url)
                    .post(requestBody)
                    .addHeader("Authorization", "Bearer " + jwtToken)
                    .build();

            Response result = client.newCall(request).execute();
            if (result.body() != null) {
                response = gson.fromJson(result.body().string(), type);
            }
        }

        return response;
    }
}
