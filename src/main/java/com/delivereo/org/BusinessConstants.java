package com.delivereo.org;

class BusinessConstants {

    private BusinessConstants() {
        // Empty constructor
    }

    static final String LOGIN_URL = "/api/protected/login/business";

    static final String TOKEN_RENEWAL_URL = "/api/protected/token-renewal/business";

    static final String VALIDATE_ADDRESS_URL = "/api/private/business-bookings/validate-address";

    static final String CALCULATE_URL = "/api/private/business-bookings/calculate";

    static final String CREATE_URL = "/api/private/business-bookings/create";

    static final String CREATE_URL_DOMICILE_PHARMACY_URL = "/api/private/business-bookings/create-domicile";

    static final String CANCEL_URL = "/api/private/business-bookings/cancel";

    static final String RETRY_URL = "/api/private/business-bookings/retry";

    static final String RATE_DRIVER_URL = "/api/private/business-bookings/rate-driver";

    static final String DETAIL_URL = "/api/private/business-bookings/detail-full";
}
