package com.delivereo.org.base;

import com.delivereo.org.enums.Language;
import lombok.Data;

@Data
public class BasicRequest {

    private String lang;

    public BasicRequest() {
        // Empty constructor
    }

    public BasicRequest(Language lang) {
        this.lang = lang.getUrl();
    }
}
