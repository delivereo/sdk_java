package com.delivereo.org.base;

import com.delivereo.org.enums.ErrorCode;
import lombok.Data;

@Data
public class BasicResponse {

    private String title;

    private String message;

    private boolean status;

    private int code;

    public BasicResponse() {
        // Empty constructor
    }

    public BasicResponse(String title, String message, boolean status, ErrorCode code) {
        this.title = title;
        this.message = message;
        this.status = status;
        this.code = code.getValue();
    }
}
