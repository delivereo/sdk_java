package com.delivereo.org.enums;

public enum CancelReason {
    NOT_NEEDED_ANYMORE,
    TOO_MUCH_DELAY,
    BETTER_OPTION,
    OTHER,
}
