package com.delivereo.org.enums;

import java.util.HashMap;
import java.util.Map;

public enum City {
    QUITO("cities.ecuador.pichincha.quito"),
    GUAYAQUIL("cities.ecuador.guayas.guayaquil"),
    SANGOLQUI("cities.ecuador.pichincha.sangolqui"),
    RIOBAMBA("cities.ecuador.chimborazo.riobamba"),
    AMBATO("cities.ecuador.tungurahua.ambato"),
    BABAHOYO("cities.ecuador.los.rios.babahoyo"),
    IBARRA("cities.ecuador.imbabura.ibarra"),
    CUENCA("cities.ecuador.azuay.cuenca"),
    MANTA("cities.ecuador.manabi.manta"),
    PORTOVIEJO("cities.ecuador.manabi.portoviejo"),
    SANTO_DOMINGO("cities.ecuador.santo.domingo.santo.domingo"),
    LATACUNGA("cities.ecuador.cotopaxi.latacunga"),
    ESMERALDAS("cities.ecuador.esmeraldas.esmeraldas"),
    QUININDE("cities.ecuador.esmeraldas.quininde"),
    PEDERNALES("cities.ecuador.manabi.pedernales"),
    LA_CONCORDIA("cities.ecuador.santo.domingo.la.concordia"),
    OTAVALO("cities.ecuador.imbabura.otavalo");

    //Lookup table
    private static final Map<String, City> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (City env : City.values()) {
            lookup.put(env.getUrl(), env);
        }
    }

    private String value;

    City(String value) {
        this.value = value;
    }

    //This method can be used for reverse lookup purpose
    public static City get(String url) {
        return lookup.get(url);
    }

    public String getUrl() {
        return value;
    }
}
