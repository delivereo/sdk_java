package com.delivereo.org.enums;

import java.util.HashMap;
import java.util.Map;

public enum ErrorCode {
    NOT_ENOUGH_DATA(401),
    INCORRECT_DATA(402),
    DATA_NOT_FOUND(404),
    FAILURE(405),
    SUCCESS(200),
    INCORRECT_CREDENTIALS(204),
    USER_MOBILE_NUMBER_EMPTY(205),
    USER_SUSPENDED(206),
    USER_DISABLED(207),
    USER_NOT_VERIFIED(208);

    private static Map<Integer, ErrorCode> map = new HashMap<>();

    static {
        for (ErrorCode bookingStatus : ErrorCode.values()) {
            map.put(bookingStatus.value, bookingStatus);
        }
    }

    private final int value;

    ErrorCode(final int newValue) {
        value = newValue;
    }

    public static ErrorCode valueOf(int bookingStatus) {
        return map.get(bookingStatus);
    }

    public int getValue() {
        return value;
    }
}
