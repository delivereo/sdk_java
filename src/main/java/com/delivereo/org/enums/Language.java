package com.delivereo.org.enums;

import java.util.HashMap;
import java.util.Map;

public enum Language {
    SPANISH("es"),
    ENGLISH("en");

    //Lookup table
    private static final Map<String, Language> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (Language env : Language.values()) {
            lookup.put(env.getUrl(), env);
        }
    }

    private String value;

    Language(String value) {
        this.value = value;
    }

    //This method can be used for reverse lookup purpose
    public static Language get(String url) {
        return lookup.get(url);
    }

    public String getUrl() {
        return value;
    }
}
