package com.delivereo.org.enums;

import java.util.HashMap;
import java.util.Map;

public enum PackageSize {
    SMALL("category.small"),
    MEDIUM("category.medium"),
    LARGE("category.large");

    //Lookup table
    private static final Map<String, PackageSize> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (PackageSize env : PackageSize.values()) {
            lookup.put(env.getUrl(), env);
        }
    }

    private String value;

    PackageSize(String value) {
        this.value = value;
    }

    //This method can be used for reverse lookup purpose
    public static PackageSize get(String url) {
        return lookup.get(url);
    }

    public String getUrl() {
        return value;
    }
}
