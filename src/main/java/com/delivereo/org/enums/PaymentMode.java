package com.delivereo.org.enums;

import java.util.HashMap;
import java.util.Map;

public enum PaymentMode {
    CASH("payment.mode.cash"),
    CREDIT_CARD("payment.mode.credit.card"),
    WALLET("payment.mode.wallet"),
    NONE("payment.mode.none"),
    CHECK("payment.mode.check"),
    AGREEMENT("payment.mode.agreement"),
    MIXED("payment.mode.mixed");

    //Lookup table
    private static final Map<String, PaymentMode> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (PaymentMode env : PaymentMode.values()) {
            lookup.put(env.getUrl(), env);
        }
    }

    private String value;

    PaymentMode(String value) {
        this.value = value;
    }

    //This method can be used for reverse lookup purpose
    public static PaymentMode get(String url) {
        return lookup.get(url);
    }

    public String getUrl() {
        return value;
    }
}
