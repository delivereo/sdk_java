package com.delivereo.org.enums;

public enum RatingLowReason {
    TOO_SLOW,
    BAD_APPEARANCE,
    BAD_BEHAVIOR,
    PACKAGE_IN_BAD_SHAPE,
    OTHER,
    NONE
}
