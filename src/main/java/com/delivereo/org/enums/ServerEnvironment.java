package com.delivereo.org.enums;

import java.util.HashMap;
import java.util.Map;

public enum ServerEnvironment {
    DEVELOPMENT("http://dgamc.ddns.net:8080"),
    TEST("http://delivereo-test-advanced.us-east-1.elasticbeanstalk.com"),
    STAGING("https://delivereo-test.com"),
    PRODUCTION("https://delivereo.com");

    //Lookup table
    private static final Map<String, ServerEnvironment> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (ServerEnvironment env : ServerEnvironment.values()) {
            lookup.put(env.getUrl(), env);
        }
    }

    private String value;

    ServerEnvironment(String value) {
        this.value = value;
    }

    //This method can be used for reverse lookup purpose
    public static ServerEnvironment get(String url) {
        return lookup.get(url);
    }

    public String getUrl() {
        return value;
    }
}
