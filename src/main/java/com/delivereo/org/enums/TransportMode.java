package com.delivereo.org.enums;

import java.util.HashMap;
import java.util.Map;

public enum TransportMode {
    NONE("none"),
    CAR("transport.mode.car"),
    MOTORCYCLE("transport.mode.motorcycle"),
    BICYCLE("transport.mode.bicycle"),
    WALK("transport.mode.walk");

    //Lookup table
    private static final Map<String, TransportMode> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (TransportMode env : TransportMode.values()) {
            lookup.put(env.getUrl(), env);
        }
    }

    private String value;

    TransportMode(String value) {
        this.value = value;
    }

    //This method can be used for reverse lookup purpose
    public static TransportMode get(String url) {
        return lookup.get(url);
    }

    public String getUrl() {
        return value;
    }
}
