package com.delivereo.org.request.calculate;

import lombok.Data;

@Data
public class CalculateAddress {

    private int addressOrder;

    private String fullAddress;

    @Deprecated
    private String addressMainStreet;

    @Deprecated
    private String addressCrossingStreet;

    private String countryCode;

    public CalculateAddress() {
        // Empty constructor
    }

    public CalculateAddress(int addressOrder, String fullAddress, @Deprecated String addressMainStreet, @Deprecated String addressCrossingStreet, String countryCode) {
        this.addressOrder = addressOrder;
        this.fullAddress = fullAddress;
        this.addressMainStreet = addressMainStreet;
        this.addressCrossingStreet = addressCrossingStreet;
        this.countryCode = countryCode;
    }
}
