package com.delivereo.org.request.calculate;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.enums.Language;
import com.delivereo.org.enums.PackageSize;
import com.delivereo.org.enums.City;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class CalculateBooking extends BasicRequest {

    private PackageSize categoryType;

    private City cityType;

    private List<CalculatePoint> points;

    private List<CalculateAddress> addresses;

    public CalculateBooking() {
        // Empty constructor
    }

    public CalculateBooking(Language lang, PackageSize categoryType, City cityType, List<CalculatePoint> points, List<CalculateAddress> addresses) {
        super(lang);
        this.categoryType = categoryType;
        this.cityType = cityType;
        this.points = points;
        this.addresses = addresses;
    }
}
