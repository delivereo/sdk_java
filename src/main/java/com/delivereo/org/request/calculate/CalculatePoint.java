package com.delivereo.org.request.calculate;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CalculatePoint {

    private int pointOrder;

    private BigDecimal pointLatitude;

    private BigDecimal pointLongitude;

    public CalculatePoint() {
        // Empty constructor
    }

    public CalculatePoint(int pointOrder, BigDecimal pointLatitude, BigDecimal pointLongitude) {
        this.pointOrder = pointOrder;
        this.pointLatitude = pointLatitude;
        this.pointLongitude = pointLongitude;
    }
}
