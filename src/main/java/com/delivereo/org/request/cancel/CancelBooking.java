package com.delivereo.org.request.cancel;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.enums.CancelReason;
import com.delivereo.org.enums.Language;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CancelBooking extends BasicRequest {

    private int bookingId;

    private CancelReason cancelReason;

    private String cancelFeedback;

    public CancelBooking() {
        // Empty constructor
    }

    public CancelBooking(Language lang, int bookingId, CancelReason cancelReason, String cancelFeedback) {
        super(lang);
        this.bookingId = bookingId;
        this.cancelReason = cancelReason;
        this.cancelFeedback = cancelFeedback;
    }
}
