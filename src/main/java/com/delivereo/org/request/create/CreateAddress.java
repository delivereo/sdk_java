package com.delivereo.org.request.create;

import lombok.Data;

@Data
public class CreateAddress {

    private int addressOrder;

    private String fullAddress;

    @Deprecated
    private String addressMainStreet;

    @Deprecated
    private String addressCrossingStreet;

    private String countryCode;

    private String senderRecipientName;

    private String address;

    private String reference;

    public CreateAddress() {
        // Empty constructor
    }

    public CreateAddress(int addressOrder, String fullAddress, @Deprecated String addressMainStreet, @Deprecated String addressCrossingStreet, String countryCode, String senderRecipientName, String address, String reference) {
        this.addressOrder = addressOrder;
        this.fullAddress = fullAddress;
        this.addressMainStreet = addressMainStreet;
        this.addressCrossingStreet = addressCrossingStreet;
        this.countryCode = countryCode;
        this.senderRecipientName = senderRecipientName;
        this.address = address;
        this.reference = reference;
    }
}
