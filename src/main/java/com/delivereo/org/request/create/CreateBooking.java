package com.delivereo.org.request.create;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.enums.Language;
import com.delivereo.org.enums.PackageSize;
import com.delivereo.org.enums.City;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class CreateBooking extends BasicRequest {

    private PackageSize categoryType;

    private City cityType;

    private List<CreatePoint> points;

    private List<CreateAddress> addresses;

    private String description;

    private String bookingImage;

    private String shopName;

    private String shopIcon;

    private BigDecimal itemsPrice;

    private String extraInstructions;

    private String maxSuggestedTime;

    private CreateOrder order;

    private String bookingBusinessUserEmail;

    public CreateBooking() {
        // Empty constructor
    }

    public CreateBooking(Language lang, PackageSize categoryType, City cityType, List<CreatePoint> points, List<CreateAddress> addresses, String description, String bookingImage, String shopName, String shopIcon, BigDecimal itemsPrice, String extraInstructions, String maxSuggestedTime, CreateOrder order, String bookingBusinessUserEmail) {
        super(lang);
        this.categoryType = categoryType;
        this.cityType = cityType;
        this.points = points;
        this.addresses = addresses;
        this.description = description;
        this.bookingImage = bookingImage;
        this.shopName = shopName;
        this.shopIcon = shopIcon;
        this.itemsPrice = itemsPrice;
        this.extraInstructions = extraInstructions;
        this.maxSuggestedTime = maxSuggestedTime;
        this.order = order;
        this.bookingBusinessUserEmail = bookingBusinessUserEmail;
    }
}
