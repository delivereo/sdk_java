package com.delivereo.org.request.create;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.enums.Language;
import com.delivereo.org.enums.PaymentMode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class CreateBookingDomicilePharmacy extends BasicRequest {

    private String businessFirstBranchName;

    private List<String> businessBranchNamesToTransfer;

    private String businessUserEmail;

    private String orderReference;

    private String clientPersonalId;

    private String clientFullName;

    private String clientAddress;

    private String clientMobileNumber;

    private String clientReference;

    private PaymentMode clientPaymentMode;

    private String countryCode;

    public CreateBookingDomicilePharmacy() {
        // Empty constructor
    }

    public CreateBookingDomicilePharmacy(Language lang, String businessFirstBranchName, List<String> businessBranchNamesToTransfer, String businessUserEmail, String orderReference, String clientPersonalId, String clientFullName, String clientAddress, String clientMobileNumber, String clientReference, PaymentMode clientPaymentMode, String countryCode) {
        super(lang);
        this.businessFirstBranchName = businessFirstBranchName;
        this.businessBranchNamesToTransfer = businessBranchNamesToTransfer;
        this.businessUserEmail = businessUserEmail;
        this.orderReference = orderReference;
        this.clientPersonalId = clientPersonalId;
        this.clientFullName = clientFullName;
        this.clientAddress = clientAddress;
        this.clientMobileNumber = clientMobileNumber;
        this.clientReference = clientReference;
        this.clientPaymentMode = clientPaymentMode;
        this.countryCode = countryCode;
    }
}