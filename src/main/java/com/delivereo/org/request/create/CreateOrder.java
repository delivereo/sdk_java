package com.delivereo.org.request.create;

import com.delivereo.org.enums.PaymentMode;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class CreateOrder {

    private String orderGuid;

    private BigDecimal orderSubTotal;

    private PaymentMode paymentMode;

    private BigDecimal orderIva;

    private BigDecimal orderTotal;

    private List<CreateOrderItem> orderItems;

    public CreateOrder() {
        // Empty constructor
    }

    public CreateOrder(String orderGuid, List<CreateOrderItem> orderItems, BigDecimal orderSubTotal, BigDecimal orderIva, BigDecimal orderTotal, PaymentMode paymentMode) {
        this.orderGuid = orderGuid;
        this.orderItems = orderItems;
        this.orderSubTotal = orderSubTotal;
        this.orderIva = orderIva;
        this.orderTotal = orderTotal;
        this.paymentMode = paymentMode;
    }
}
