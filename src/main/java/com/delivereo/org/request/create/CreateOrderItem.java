package com.delivereo.org.request.create;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CreateOrderItem {

    private String orderItemGuid;

    private String orderItemName;

    private String orderItemDescription;

    private long quantity;

    private BigDecimal unitSubTotal;

    private BigDecimal unitIva;

    private BigDecimal unitTotal;

    private BigDecimal orderItemPriceTotal;

    public CreateOrderItem() {
        // Empty constructor
    }

    public CreateOrderItem(String orderItemGuid, String orderItemName, String orderItemDescription, long quantity, BigDecimal unitSubTotal, BigDecimal unitIva, BigDecimal unitTotal, BigDecimal orderItemPriceTotal) {
        this.orderItemGuid = orderItemGuid;
        this.orderItemName = orderItemName;
        this.orderItemDescription = orderItemDescription;
        this.quantity = quantity;
        this.unitSubTotal = unitSubTotal;
        this.unitIva = unitIva;
        this.unitTotal = unitTotal;
        this.orderItemPriceTotal = orderItemPriceTotal;
    }
}
