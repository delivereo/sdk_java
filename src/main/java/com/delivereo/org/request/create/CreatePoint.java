package com.delivereo.org.request.create;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CreatePoint {

    private int pointOrder;

    private String senderRecipientName;

    private String address;

    private String reference;

    private BigDecimal pointLatitude;

    private BigDecimal pointLongitude;

    public CreatePoint() {
        // Empty constructor
    }

    public CreatePoint(int pointOrder,  BigDecimal pointLatitude, BigDecimal pointLongitude, String senderRecipientName, String address, String reference) {
        this.pointOrder = pointOrder;
        this.pointLatitude = pointLatitude;
        this.pointLongitude = pointLongitude;
        this.senderRecipientName = senderRecipientName;
        this.address = address;
        this.reference = reference;
    }
}
