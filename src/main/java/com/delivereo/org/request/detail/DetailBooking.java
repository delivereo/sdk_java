package com.delivereo.org.request.detail;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.enums.Language;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DetailBooking extends BasicRequest {

    private int bookingId;

    public DetailBooking() {
        // Empty constructor
    }

    public DetailBooking(Language lang, int bookingId) {
        super(lang);
        this.bookingId = bookingId;
    }
}
