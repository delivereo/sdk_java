package com.delivereo.org.request.login;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.enums.Language;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SignIn extends BasicRequest {

    private String apiKey;

    private String email;

    private String ruc;

    public SignIn() {
        // Empty constructor
    }

    public SignIn(Language lang, String apiKey, String email, String ruc) {
        super(lang);
        this.apiKey = apiKey;
        this.email = email;
        this.ruc = ruc;
    }
}
