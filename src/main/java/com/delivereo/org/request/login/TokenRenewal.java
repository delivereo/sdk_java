package com.delivereo.org.request.login;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.enums.Language;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TokenRenewal extends BasicRequest {

    private String oldJwtToken;

    private String email;

    public TokenRenewal() {
        // Empty constructor
    }

    public TokenRenewal(Language lang, String oldJwtToken, String email) {
        super(lang);
        this.oldJwtToken = oldJwtToken;
        this.email = email;
    }
}
