package com.delivereo.org.request.rate;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.enums.Language;
import com.delivereo.org.enums.RatingLowReason;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
public class RateDriver extends BasicRequest {

    private int bookingId;

    private BigDecimal rating;

    private RatingLowReason ratingLowReasonType;

    private String ratingLowFeedback;

    public RateDriver() {
        // Empty constructor
    }

    public RateDriver(Language lang, int bookingId, BigDecimal rating, RatingLowReason ratingLowReasonType, String ratingLowFeedback) {
        super(lang);
        this.bookingId = bookingId;
        this.rating = rating;
        this.ratingLowReasonType = ratingLowReasonType;
        this.ratingLowFeedback = ratingLowFeedback;
    }
}
