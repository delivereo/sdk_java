package com.delivereo.org.request.retry;

import com.delivereo.org.base.BasicRequest;
import com.delivereo.org.enums.Language;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class RetryBooking extends BasicRequest {

    private int bookingId;

    public RetryBooking() {
        // Empty constructor
    }

    public RetryBooking(Language lang, int bookingId) {
        super(lang);
        this.bookingId = bookingId;
    }
}
