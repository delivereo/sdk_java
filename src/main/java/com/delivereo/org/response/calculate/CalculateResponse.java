package com.delivereo.org.response.calculate;

import com.delivereo.org.base.BasicResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
public class CalculateResponse extends BasicResponse {

    private BigDecimal farePrice;

    private BigDecimal itemsPrice;

    private BigDecimal ivaPercentage;

    private BigDecimal iva;

    private BigDecimal totalAmount;

    private String estimatedTime;

    public CalculateResponse(BasicResponse basicResponse) {
        this.setTitle(basicResponse.getTitle());
        this.setMessage(basicResponse.getMessage());
        this.setStatus(basicResponse.isStatus());
        this.setCode(basicResponse.getCode());
    }

    public CalculateResponse(BasicResponse basicResponse, BigDecimal farePrice, BigDecimal itemsPrice, BigDecimal ivaPercentage, BigDecimal iva, BigDecimal totalAmount, String estimatedTime) {
        this.setTitle(basicResponse.getTitle());
        this.setMessage(basicResponse.getMessage());
        this.setStatus(basicResponse.isStatus());
        this.setCode(basicResponse.getCode());

        this.farePrice = farePrice;
        this.itemsPrice = itemsPrice;
        this.ivaPercentage = ivaPercentage;
        this.iva = iva;
        this.totalAmount = totalAmount;
        this.estimatedTime = estimatedTime;
    }
}
