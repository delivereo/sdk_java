package com.delivereo.org.response.create;

import com.delivereo.org.base.BasicResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CreateResponse extends BasicResponse {

    private long bookingId;

    public CreateResponse(BasicResponse basicResponse) {
        this.setTitle(basicResponse.getTitle());
        this.setMessage(basicResponse.getMessage());
        this.setStatus(basicResponse.isStatus());
        this.setCode(basicResponse.getCode());
    }

    public CreateResponse(BasicResponse basicResponse, int bookingId) {
        this.setTitle(basicResponse.getTitle());
        this.setMessage(basicResponse.getMessage());
        this.setStatus(basicResponse.isStatus());
        this.setCode(basicResponse.getCode());

        this.bookingId = bookingId;
    }
}
