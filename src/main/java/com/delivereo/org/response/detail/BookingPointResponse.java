package com.delivereo.org.response.detail;

import lombok.Data;

import java.util.Date;

@Data
public class BookingPointResponse {

    private int pointId;

    private int pointOrder;

    private String senderRecipientName;

    private String address;

    private String reference;

    private Date pointInitialTime;

    private Date pointArrivalTime;

    public BookingPointResponse() {
        // Empty constructor
    }

    public BookingPointResponse(int pointId, int pointOrder, String senderRecipientName, String address, String reference, Date pointInitialTime, Date pointArrivalTime) {
        this.pointId = pointId;
        this.pointOrder = pointOrder;
        this.senderRecipientName = senderRecipientName;
        this.address = address;
        this.reference = reference;
        this.pointInitialTime = pointInitialTime;
        this.pointArrivalTime = pointArrivalTime;
    }
}
