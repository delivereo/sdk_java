

package com.delivereo.org.response.detail;

import com.delivereo.org.base.BasicResponse;
import com.delivereo.org.enums.BookingStatus;
import com.delivereo.org.enums.TransportMode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class BookingResponse extends BasicResponse {

    private int bookingId;

    private long userId;

    private long generalUserId;

    private Long driverId;

    private String driverFirstName;

    private String driverLastName;

    private String driverEmail;

    private String driverMobileNumber;

    private String driverRegistrationNumber;

    private TransportMode driverTransport;

    private String driverTransportModeName;

    private String firstPointSender;

    private String firstPointAddress;

    private Date firstPointInitialTime;

    private Date firstPointArrivalTime;

    private String lastPointRecipient;

    private String lastPointAddress;

    private Date lastPointInitialTime;

    private Date lastPointArrivalTime;

    private BigDecimal fare;

    private String description;

    private boolean receiptStatus;

    private BookingStatus bookingStatus;

    private String bookingStatusName;

    private BigDecimal itemsPrice;

    private BigDecimal insuredAmount;

    private BigDecimal iva;

    private BigDecimal totalAmount;

    private String storeName;

    private String otherImage;

    private String totalDuration;

    private String extraInstructions;

    private Long currentExtraPointId;

    private String publicGuid;

    private String publicUrl;

    private List<BookingPointResponse> extraPoints;

    public BookingResponse() {
        // Empty constructor
    }

    public BookingResponse(BasicResponse basicResponse) {
        this.setTitle(basicResponse.getTitle());
        this.setMessage(basicResponse.getMessage());
        this.setStatus(basicResponse.isStatus());
        this.setCode(basicResponse.getCode());
    }

    public BookingResponse(BasicResponse basicResponse, int bookingId, long userId, long generalUserId, Long driverId, String driverFirstName, String driverLastName, String driverEmail, String driverMobileNumber, String driverRegistrationNumber, TransportMode driverTransport, String driverTransportModeName, String firstPointSender, String firstPointAddress, Date firstPointInitialTime, Date firstPointArrivalTime, String lastPointRecipient, String lastPointAddress, Date lastPointInitialTime, Date lastPointArrivalTime, BigDecimal fare, String description, BookingStatus bookingStatus, String bookingStatusName, BigDecimal itemsPrice, BigDecimal insuredAmount, BigDecimal iva, BigDecimal totalAmount, String storeName, String otherImage, String totalDuration, String extraInstructions, Long currentExtraPointId, String publicGuid, String publicUrl, List<BookingPointResponse> extraPoints) {
        this.setTitle(basicResponse.getTitle());
        this.setMessage(basicResponse.getMessage());
        this.setStatus(basicResponse.isStatus());
        this.setCode(basicResponse.getCode());

        this.bookingId = bookingId;
        this.userId = userId;
        this.generalUserId = generalUserId;
        this.driverId = driverId;
        this.driverFirstName = driverFirstName;
        this.driverLastName = driverLastName;
        this.driverEmail = driverEmail;
        this.driverMobileNumber = driverMobileNumber;
        this.driverRegistrationNumber = driverRegistrationNumber;
        this.driverTransport = driverTransport;
        this.driverTransportModeName = driverTransportModeName;
        this.firstPointSender = firstPointSender;
        this.firstPointAddress = firstPointAddress;
        this.firstPointInitialTime = firstPointInitialTime;
        this.firstPointArrivalTime = firstPointArrivalTime;
        this.lastPointRecipient = lastPointRecipient;
        this.lastPointAddress = lastPointAddress;
        this.lastPointInitialTime = lastPointInitialTime;
        this.lastPointArrivalTime = lastPointArrivalTime;
        this.fare = fare;
        this.description = description;
        this.bookingStatus = bookingStatus;
        this.bookingStatusName = bookingStatusName;
        this.itemsPrice = itemsPrice;
        this.insuredAmount = insuredAmount;
        this.iva = iva;
        this.totalAmount = totalAmount;
        this.storeName = storeName;
        this.otherImage = otherImage;
        this.totalDuration = totalDuration;
        this.extraInstructions = extraInstructions;
        this.currentExtraPointId = currentExtraPointId;
        this.publicGuid = publicGuid;
        this.publicUrl = publicUrl;
        this.extraPoints = extraPoints;
    }
}