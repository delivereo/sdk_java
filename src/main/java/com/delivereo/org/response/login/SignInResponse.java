package com.delivereo.org.response.login;

import com.delivereo.org.base.BasicResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
public class SignInResponse extends BasicResponse {

    private int id;

    private String ruc;

    private String businessName;

    private String email;

    private String countryCode;

    private String mobileNumber;

    private String businessImage;

    private BigDecimal walletBalance;

    private String jwtToken;

    public SignInResponse() {
        // Empty constructor
    }

    public SignInResponse(BasicResponse basicResponse) {
        this.setTitle(basicResponse.getTitle());
        this.setMessage(basicResponse.getMessage());
        this.setStatus(basicResponse.isStatus());
        this.setCode(basicResponse.getCode());
    }

    public SignInResponse(BasicResponse basicResponse, int id, String ruc, String businessName, String email, String countryCode, String mobileNumber, String businessImage, BigDecimal walletBalance) {
        this.setTitle(basicResponse.getTitle());
        this.setMessage(basicResponse.getMessage());
        this.setStatus(basicResponse.isStatus());
        this.setCode(basicResponse.getCode());

        this.id = id;
        this.ruc = ruc;
        this.businessName = businessName;
        this.email = email;
        this.countryCode = countryCode;
        this.mobileNumber = mobileNumber;
        this.businessImage = businessImage;
        this.walletBalance = walletBalance;
    }
}
