package com.delivereo.org.response.login;

import com.delivereo.org.base.BasicResponse;
import com.delivereo.org.enums.ErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TokenRenewalResponse extends BasicResponse {

    private String result;

    public TokenRenewalResponse() {
        // Empty constructor
    }

    public TokenRenewalResponse(BasicResponse basicResponse) {
        super(basicResponse.getTitle(), basicResponse.getMessage(), basicResponse.isStatus(), ErrorCode.valueOf(basicResponse.getCode()));
    }

    public TokenRenewalResponse(BasicResponse basicResponse, String result) {
        super(basicResponse.getTitle(), basicResponse.getMessage(), basicResponse.isStatus(), ErrorCode.valueOf(basicResponse.getCode()));
        this.result = result;
    }
}
