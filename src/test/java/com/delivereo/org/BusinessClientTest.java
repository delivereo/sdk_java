package com.delivereo.org;

import com.delivereo.org.base.BasicResponse;
import com.delivereo.org.enums.*;
import com.delivereo.org.request.calculate.CalculateAddress;
import com.delivereo.org.request.calculate.CalculatePoint;
import com.delivereo.org.request.create.CreateAddress;
import com.delivereo.org.request.create.CreateOrder;
import com.delivereo.org.request.create.CreateOrderItem;
import com.delivereo.org.request.create.CreatePoint;
import com.delivereo.org.response.calculate.CalculateResponse;
import com.delivereo.org.response.create.CreateResponse;
import com.delivereo.org.response.detail.BookingResponse;
import com.delivereo.org.response.login.SignInResponse;
import com.delivereo.org.response.login.TokenRenewalResponse;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class BusinessClientTest {

    private BusinessClient businessClient = new BusinessClient(ServerEnvironment.DEVELOPMENT, Language.SPANISH);

    @Test
    public void login() {
        try {
            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");

            Assert.assertNotNull(loginResponse);
            Assert.assertTrue(loginResponse.isStatus());
            Assert.assertNotNull(loginResponse.getJwtToken());
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void tokenRenewal() {
        try {
            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            TokenRenewalResponse renewTokenResponse = businessClient.tokenRenewal(loginResponse.getJwtToken(), "info@delivereo.com");

            Assert.assertNotNull(renewTokenResponse);
            if (renewTokenResponse.getMessage().equals("Token todavia es valido")) {
                Assert.assertTrue(true);
            } else {
                Assert.assertNotNull(renewTokenResponse.getResult());
            }
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void calculateBookingCostUsingPoints() {
        try {
            List<CalculatePoint> points = new LinkedList<>();
            points.add(new CalculatePoint(1, BigDecimal.valueOf(-0.19045013), BigDecimal.valueOf(-78.48059853)));
            points.add(new CalculatePoint(2, BigDecimal.valueOf(-0.172213), BigDecimal.valueOf(-78.486467)));
            points.add(new CalculatePoint(3, BigDecimal.valueOf(-0.20963146), BigDecimal.valueOf(-78.43948563)));

            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            CalculateResponse calculateResponse = businessClient.calculateBookingCostUsingPoints(loginResponse.getJwtToken(), City.QUITO, points);

            Assert.assertNotNull(calculateResponse);
            Assert.assertTrue(calculateResponse.isStatus());
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void calculateBookingCostUsingAddresses() {
        try {
            List<CalculateAddress> addresses = new LinkedList<>();
            addresses.add(new CalculateAddress(1, "Amazonas y Veintimilla", "", "", "EC"));
            addresses.add(new CalculateAddress(2, "Francisco Dalmau y Calle 1", "", "", "EC"));

            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            CalculateResponse calculateResponse = businessClient.calculateBookingCostUsingAddresses(loginResponse.getJwtToken(), City.QUITO, addresses);

            Assert.assertNotNull(calculateResponse);
            Assert.assertTrue(calculateResponse.isStatus());
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void createBookingUsingPoints() {
        try {
            List<CreatePoint> points = new LinkedList<>();
            points.add(new CreatePoint(1, BigDecimal.valueOf(-0.19045013), BigDecimal.valueOf(-78.48059853), "Jorge Cardenas", "Amazonas y Veintimilla E32A-32", "Edificio Tez Apartamento 200"));
            points.add(new CreatePoint(2, BigDecimal.valueOf(-0.172213), BigDecimal.valueOf(-78.486467), "Marcelo Bonilla", "Diego de Vazquez y Bellavista E34-23", "Condominio Carmen 2 casa 34"));
            points.add(new CreatePoint(3, BigDecimal.valueOf(-0.20963146), BigDecimal.valueOf(-78.43948563), "Pablo Mancero", "Naciones Unidas e Inaquito Esquina", "Edificio Metropolitan Oficina 409"));

            CreateOrder createOrder = new CreateOrder(
                    "123456",
                    new LinkedList<CreateOrderItem>(),
                    BigDecimal.valueOf(10),
                    BigDecimal.valueOf(1.2),
                    BigDecimal.valueOf(11.2),
                    PaymentMode.CREDIT_CARD
            );

            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            CreateResponse createResponse = businessClient.createBookingUsingPoints(loginResponse.getJwtToken(), City.QUITO, points,
                    "Compra hamburguesa de pollo y hamburguesa de carne", BigDecimal.valueOf(11.2), createOrder,
                    "", "", "", "");

            Assert.assertNotNull(createResponse);
            Assert.assertTrue(createResponse.isStatus());
            Assert.assertNotEquals(0, createResponse.getBookingId());
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void createBookingUsingAddresses() {
        try {
            List<CreateAddress> addresses = new LinkedList<>();
            addresses.add(new CreateAddress(1, "Amazonas y Veintimilla", "", "", "EC", "Jorge Cardenas", "Amazonas y Veintimilla E32A-32", "Edificio Tez Apartamento 200"));
            addresses.add(new CreateAddress(2, "Francisco Dalmau y Calle 1", "", "", "EC", "Marcelo Bonilla", "Diego de Vazquez y Bellavista E34-23", "Condominio Carmen 2 casa 34"));

            CreateOrder createOrder = new CreateOrder(
                    "123456",
                    new LinkedList<CreateOrderItem>(),
                    BigDecimal.valueOf(10),
                    BigDecimal.valueOf(1.2),
                    BigDecimal.valueOf(11.2),
                    PaymentMode.CREDIT_CARD
            );

            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            CreateResponse createResponse = businessClient.createBookingUsingAddresses(loginResponse.getJwtToken(), City.QUITO, addresses,
                    "Compra hamburguesa de pollo y hamburguesa de carne", BigDecimal.valueOf(11.2), createOrder,
                    "", "", "", "");

            Assert.assertNotNull(createResponse);
            Assert.assertTrue(createResponse.isStatus());
            Assert.assertNotEquals(0, createResponse.getBookingId());
        } catch (IOException e) {
            Assert.fail();
        }
    }

    //@Test
    public void createBookingDomicile() {
        try {
            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            CreateResponse createBookingDomicileResponse = businessClient.CreateBookingDomicilePharmacy(loginResponse.getJwtToken(), "matriz",
                    new LinkedList<>(), "info@delivereo.com", "12345", "1715245933", "Daniel Mancero",
                    "Amazonas y naciones unidas", "+593-123456789", "Edificio Argos", PaymentMode.CREDIT_CARD, "EC");

            Assert.assertNotNull(createBookingDomicileResponse);
            Assert.assertTrue(createBookingDomicileResponse.isStatus());
            Assert.assertNotEquals(0, createBookingDomicileResponse.getBookingId());
        } catch (IOException e) {
            Assert.fail();
        }
    }

    //@Test
    public void createBookingDomicilePharmacy() {
        try {
            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            CreateResponse createBookingDomicilePharmacyResponse = businessClient.CreateBookingDomicilePharmacy(loginResponse.getJwtToken(), "matriz",
                    new LinkedList<>(Collections.singletonList("otra")), "info@delivereo.com", "12345", "1715245933", "Daniel Mancero",
                    "Amazonas y naciones unidas", "+593-123456789", "Edificio Argos", PaymentMode.CREDIT_CARD, "EC");

            Assert.assertNotNull(createBookingDomicilePharmacyResponse);
            Assert.assertTrue(createBookingDomicilePharmacyResponse.isStatus());
            Assert.assertNotEquals(0, createBookingDomicilePharmacyResponse.getBookingId());
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void cancelBookingById() {
        try {
            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            BasicResponse basicResponse = businessClient.cancelBookingById(
                    loginResponse.getJwtToken(),
                    36762,
                    CancelReason.NOT_NEEDED_ANYMORE,
                    "Another option was found");

            Assert.assertNotNull(basicResponse);
            if (basicResponse.getCode() == 405) {
                Assert.assertTrue(true);
            } else {
                Assert.assertTrue(basicResponse.isStatus());
            }
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void retryBookingById() {
        try {
            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            BasicResponse basicResponse = businessClient.retryBookingById(
                    loginResponse.getJwtToken(),
                    36762);

            Assert.assertNotNull(basicResponse);
            if (basicResponse.getCode() == 405) {
                Assert.assertTrue(true);
            } else {
                Assert.assertTrue(basicResponse.isStatus());
            }
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void rateDriverForBookingById() {
        try {
            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            BasicResponse basicResponse = businessClient.rateDriverForBookingById(
                    loginResponse.getJwtToken(),
                    36762,
                    BigDecimal.valueOf(5.0),
                    RatingLowReason.NONE,
                    "Everything OK");

            Assert.assertNotNull(basicResponse);
            if (basicResponse.getCode() == 405) {
                Assert.assertTrue(true);
            } else {
                Assert.assertTrue(basicResponse.isStatus());
            }
        } catch (IOException e) {
            Assert.fail();
        }
    }

    @Test
    public void getBookingFullDetailById() {
        try {
            SignInResponse loginResponse = businessClient.login("69982032-DD5", "info@delivereo.com", "1792715083001");
            BookingResponse bookingResponse = businessClient.getBookingFullDetailById(
                    loginResponse.getJwtToken(),
                    36762);

            Assert.assertNotNull(bookingResponse);
            Assert.assertTrue(bookingResponse.isStatus());
        } catch (IOException e) {
            Assert.fail();
        }
    }
}
